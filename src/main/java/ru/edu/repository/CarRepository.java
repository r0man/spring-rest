package ru.edu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.entity.Car;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    List<Car> findAllByName(String name);
    List<Car> findAllByNameAndYear(String name, Integer year);

    //@Query("select new Car (c.id, c.name) from Car c where c.year < :year")
    @Query(value = "select * from cars where year < :year", nativeQuery = true)
    List<Car> findOldCars(@Param("year") int year);

}
